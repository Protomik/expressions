#include "expressions/Expression.h"
#include "expressions/Sum.h"
#include "expressions/Subtraction.h"
#include "expressions/Multiplication.h"
#include "expressions/Division.h"
#include "expressions/Variable.h"
#include "expressions/Number.h"


int main(int argc, char ** argv) {

	Expression *sum = new Sum(new Variable("x"), new Number(3));
	std::cout << "SUM:" << std::endl;
	std::cout << "print() = " << sum->print() << std::endl;
	std::cout << "diff()  = " << sum->diff()->print() << std::endl;
	std::cout << std::endl;

	Expression * subtraction = new Subtraction(new Number(12), new Variable("x"));
	std::cout << "SUBTRACTION:" << std::endl;
	std::cout << "print() = " << subtraction->print() << std::endl;
	std::cout << "diff()  = " << subtraction->diff()->print() << std::endl;
	std::cout << std::endl;

	Expression * multiplication = new Multiplication(new Variable("x"), new Variable("y"));
	std::cout << "MULTIPLICATION:" << std::endl;
	std::cout << "print() = " << multiplication->print() << std::endl;
	std::cout << "diff()  = " << multiplication->diff()->print() << std::endl;
	std::cout << std::endl;

	Expression * division  = new Division(new Variable("x"), new Variable("y"));
	std::cout << "DIVISION:" << std::endl;
	std::cout << "print() = " << division->print() << std::endl;
	std::cout << "diff()  = " << division->diff()->print() << std::endl;
	std::cout << std::endl;
	
	return 0;
}
