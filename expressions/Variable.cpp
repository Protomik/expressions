#include "Variable.h"
#include "Number.h"

Variable::Variable (std::string x) {
	this->x = x;
};

Expression * Variable::diff() const {
	return new Number(1);
};

const std::string Variable::print() const {
	return x;		
};


