#ifndef DIVISION_H
#define DIVISION_H

#include "Expression.h"

class Division: public Expression {
public:
	Division(Expression * firstOperand, Expression * secondOperand);

	Expression * diff() const;

	const std::string print() const;

	~Division();

private:
	Expression * firstOperand;
	Expression * secondOperand;
};

#endif;