#include "Sum.h"


Sum::Sum (Expression * firstOperand, Expression * secondOperand) {
	this->firstOperand = firstOperand;
	this->secondOperand = secondOperand;
};

Sum::~Sum() {
	std::cout << "SUM DELETED" << std::endl;
	delete firstOperand;
	delete secondOperand;
};

Expression * Sum::diff() const {
	return new Sum(firstOperand->diff(), secondOperand->diff());
};

const std::string Sum::print() const {
	return firstOperand->print() + " + " + secondOperand->print();
};

