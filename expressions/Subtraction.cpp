#include "Subtraction.h"


Subtraction::Subtraction (Expression * firstOperand, Expression * secondOperand) {
	this->firstOperand = firstOperand;
	this->secondOperand = secondOperand;
};

Subtraction::~Subtraction() {
	delete firstOperand;
	delete secondOperand;
};

Expression * Subtraction::diff() const {
	return new Subtraction(firstOperand->diff(),secondOperand->diff());
};

const std::string Subtraction::print() const {
		return firstOperand->print() + " - " + secondOperand->print();
};

