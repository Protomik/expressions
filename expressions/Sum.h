#ifndef SUM_H
#define SUM_H

#include "Expression.h"

class Sum: public Expression {
public:
	Sum(Expression * firstOperand, Expression * secondOperand);

	Expression * diff() const;

	const std::string print() const;

	~Sum();
private:
	Expression * firstOperand;
	Expression * secondOperand;
};

#endif