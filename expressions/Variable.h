#ifndef VARIABLE_H
#define VARIABLE_H

#include "Expression.h"

class Variable: public Expression {
public:
	Variable(std::string x);

	Expression * diff() const;

	const std::string print() const;

private:
	std::string x;
};

#endif