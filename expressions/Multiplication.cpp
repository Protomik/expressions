#include "Multiplication.h"
#include "Sum.h"


Multiplication::Multiplication (Expression * firstOperand, Expression * secondOperand) {
	this->firstOperand = firstOperand;
	this->secondOperand = secondOperand;
};

Multiplication::~Multiplication() {
	delete firstOperand;
	delete secondOperand;
};

Expression * Multiplication::diff() const {
	return new Sum(new Multiplication(firstOperand->diff(), secondOperand), new Multiplication(firstOperand, secondOperand->diff()));
};

const std::string Multiplication::print() const {
		return firstOperand->print() + " * " + secondOperand->print();
};

