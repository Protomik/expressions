#include "Division.h"
#include "Multiplication.h"
#include "Subtraction.h"


Division::Division (Expression * firstOperand, Expression * secondOperand) {
	this->firstOperand = firstOperand;
	this->secondOperand = secondOperand;
};

Division::~Division() {
	delete firstOperand;
	delete secondOperand;
};

Expression * Division::diff() const {
	return new Division(new Subtraction(new Multiplication(firstOperand->diff(), secondOperand), new Multiplication(firstOperand, secondOperand->diff())), new Multiplication(secondOperand, secondOperand));
};

const std::string Division::print() const {
		return firstOperand->print() + " / " + secondOperand->print();
};
