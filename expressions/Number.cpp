#include "Number.h"
#include <sstream>


Number::Number(double value) {
	this->value = value;
};

Expression * Number::diff() const {
	return new Number(0);
};

const std::string Number::print() const {
	std::ostringstream result;
	result << value;
	std::string s = result.str();
	 return s;
};

