#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <string>
#include <iostream>

class Expression {
public:
	virtual Expression * diff() const = 0;

	virtual const std::string print() const = 0;

	virtual ~Expression();
};

#endif