#ifndef NUMBER_H
#define NUMBER_H

#include "Expression.h"

class Number: public Expression {
public:
	Number(double value);

	Expression * diff() const;

	const std::string print() const;

private:
	double value;
};

#endif