#ifndef SUBTRACTION_H
#define SUBTRACTION_H

#include "Expression.h"

class Subtraction :public Expression {
public:
	Subtraction(Expression * firstOperand, Expression * secondOperand);

	Expression * diff() const;

	const std::string print() const;

	~Subtraction();

private:
	Expression * firstOperand;
	Expression * secondOperand;
};

#endif