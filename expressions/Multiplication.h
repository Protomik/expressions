#ifndef MULTIPLICATION_H
#define MULTIPLICATION_H

#include "Expression.h"

class Multiplication: public Expression {
public:
	Multiplication(Expression * firstOperand, Expression * secondOperand);

	Expression * diff() const;

	const std::string print() const;

	~Multiplication();

private:
	Expression * firstOperand;
	Expression * secondOperand;
};

#endif;